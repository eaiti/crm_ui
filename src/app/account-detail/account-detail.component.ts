import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { AccountService } from '../services/account.service';
import { Account } from '../models/account';
import { MessageService } from '../services/message.service';
import { buildAddressCityStateZipcode } from '../util/common.util';


@Component({
  selector: 'app-account-detail',
  templateUrl: './account-detail.component.html',
  styleUrls: ['./account-detail.component.scss']
})
export class AccountDetailComponent implements OnInit {
  account: Account;
  buildAddress = buildAddressCityStateZipcode;

  constructor(
    private route: ActivatedRoute,
    private messageService: MessageService,
    private accountService: AccountService,
    private location: Location
  ) {}

  ngOnInit(): void {
    this.getAccount();
  }

  getAccount(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.accountService.getAccount(id).subscribe(
      account => {
        this.account = account;
      },
      response => {
        response.error.errors.forEach(e => {
          this.messageService.add(e.errorMessage);
        });
      }
    );
  }

  detailArray(account: object): [string, any][] {
    return Object.entries(account);
  }

  goBack(): void {
    this.location.back();
  }
}
