import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { AccountDetailComponent } from './account-detail/account-detail.component';
import { AccountsComponent } from './accounts/accounts.component';
import { MessagesComponent } from './messages/messages.component';

import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { LoginComponent } from './login/login.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatCardModule } from '@angular/material/card';
import { MatTabsModule, matTabsAnimations } from '@angular/material/tabs';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MeetingComponent } from './meeting/meeting.component';
import { CallComponent } from './call/call.component';
import { ContactsComponent } from './contacts/contacts.component';
import { MatRadioModule } from '@angular/material/radio';
import { EmailComponent } from './email/email.component';
import { ContactListComponent } from './contact-list/contact-list.component';
import { ContactDetailComponent } from './contact-detail/contact-detail.component';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { LoadingSpinnerComponent } from './loading-spinner/loading-spinner.component';
import { ErrorComponent } from './error/error.component';
import { HttpInterceptorService } from './services/http-interceptor.service';
import { MatListModule } from '@angular/material/list';
import { OpportunityComponent } from './opportunity/opportunity.component';
import {MatSelectModule} from '@angular/material/select';
import { OpportunityListComponent } from './opportunity-list/opportunity-list.component';
import { OpportunityDetailComponent } from './opportunity-detail/opportunity-detail.component';


@NgModule({
  declarations: [
    AppComponent,
    AccountsComponent,
    AccountDetailComponent,
    MessagesComponent,
    LoginComponent,
    MeetingComponent,
    CallComponent,
    ContactsComponent,
    EmailComponent,
    ContactListComponent,
    ContactDetailComponent,
    LoadingSpinnerComponent,
    ErrorComponent,
    OpportunityComponent,
    OpportunityListComponent,
    OpportunityDetailComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatPaginatorModule,
    MatFormFieldModule,
    MatInputModule,
    MatCardModule,
    MatTabsModule,
    MatExpansionModule,
    MatSnackBarModule,
    MatRadioModule,
    MatProgressSpinnerModule,
    MatListModule,
    MatSelectModule,
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpInterceptorService,
      multi: true}
    ],
  bootstrap: [AppComponent]
})
export class AppModule { }
