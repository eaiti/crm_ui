import { SalesRep } from './sales.rep';
import { Account } from './account';

export interface Opportunity {
  id: string;
  name: string;
  dateEntered: Date;
  description: string;
  assignedUserId: string;
  opportunityType: string;
  campaignId: string;
  leadSource: string;
  amount: number;
  amountUsdollar: number;
  dateClosedTimestamp: number;
  nextStep: string;
  salesStage: string;
  salesStatus: string;
  probability: number;
  bestCase: number;
  worstCase: number;
  salesRep: SalesRep;
  relatedAccounts: Account[];
}
