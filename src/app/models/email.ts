import { EmailAddress } from './email.address';
export interface Email {
  id: string;
  createdBy: string;
  dateSent: Date;
  name: string;
  type: string;
  status: string;
  intent: string;
  parentId: string;
  state: string;
  totalAttachments: number;
  direction: string;
  emailAddresses: EmailAddress[];
}
