export interface Page<T> {
    content: T[];
    pageable: Pageable;
    totalPages: number;
    totalElements: number;
    last: boolean;
    size: number;
    numberOfElements: number;
    number?: number;
    first: boolean;
    sort: SortTable;
    empty: boolean;
}
export interface Pageable {
    sort: SortTable;
    offset: number;
    pageSize: number;
    pageNumber: number;
    unpaged: boolean;
    paged: boolean;
}
export interface SortTable {
    sorted: boolean;
    unsorted: boolean;
    empty: boolean;
}
