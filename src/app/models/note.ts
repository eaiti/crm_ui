export interface Note {
  id: string;
  name: string;
  dateEntered: Date;
  dateModified: Date;
  description: string;
  assignedUserId: string;
  contactId: string;
  emailType: string;
  emailId: string;
}
