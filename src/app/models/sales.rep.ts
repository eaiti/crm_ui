export interface SalesRep {
    firstName: string;
    lastName: string;
    title: string;
}
