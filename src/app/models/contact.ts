import { Account } from './account';

export interface Contact {
    id: string;
    description: string;
    salutation: string;
    firstName: string;
    lastName: string;
    title: string;
    department: string;
    doNotCall: boolean;
    phoneHome: string;
    phoneMobile: string;
    phoneWork: string;
    phoneOther: string;
    phoneFax: string;
    primaryAddressStreet: string;
    primaryAddressCity: string;
    primaryAddressState: string;
    primaryAddressPostalcode: string;
    primaryAddressCountry: string;
    altAddressStreet: string;
    altAddressCity: string;
    altAddressState: string;
    altAddressPostalcode: string;
    altAddressCountry: string;
    birthdate: Date;
    assignedUserId: string;
    relatedAccounts: Account[];
}
