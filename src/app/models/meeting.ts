import { Note } from './note';
export interface Meeting {
  id: string;
  name: string;
  description: string;
  assignedUserId: string;
  location: string;
  durationHours: number;
  durationMinutes: number;
  dateStart: Date;
  dateEnd: Date;
  status: string;
  type: string;
  noteList: Note[];
}
