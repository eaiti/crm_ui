export interface ErrorMessage {
    field: string;
    errorMessage: string;
}
