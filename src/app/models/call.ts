import { Note } from './note';
export interface Call {
  id: string;
  name: string;
  description: string;
  durationHours: number;
  durationMinutes: number;
  dateStart: Date;
  dateEnd: Date;
  status: string;
  direction: string;
  outlookId: string;
  assignedUserId: string;
  noteList: Note[];
}
