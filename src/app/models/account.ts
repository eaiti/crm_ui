export interface Account {
    id: string;
    name: string;
    description: string;
    accountType: string;
    industry: string;
    annualRevenue: string;
    phoneFax: string;
    billingAddressStreet: string;
    billingAddressCity: string;
    billingAddressState: string;
    billingAddressPostalcode: string;
    billingAddressCountry: string;
    phoneOffice: string;
    phoneAlternate: string;
    website: string;
    ownership: string;
    employees: string;
    shippingAddressStreet: string;
    shippingAddressCity: string;
    shippingAddressState: string;
    shippingAddressPostalcode: string;
    shippingAddressCountry: string;
    assignedUserId: string;
    salesRepName: string;
}
