import { Component, OnInit } from '@angular/core';
import { Opportunity } from '../models/opportunity';
import { OpportunityService } from '../services/opportunity.service';
import { MessageService } from '../services/message.service';
import { ActivatedRoute } from '@angular/router';
import { buildSalesRepName } from '../util/common.util';

@Component({
  selector: 'app-opportunity-detail',
  templateUrl: './opportunity-detail.component.html',
  styleUrls: ['./opportunity-detail.component.scss'],
})
export class OpportunityDetailComponent implements OnInit {
  buildSalesRepName = buildSalesRepName;
  opportunity: Opportunity;

  constructor(
    private route: ActivatedRoute,
    private messageService: MessageService,
    private opportunityService: OpportunityService
  ) {}

  ngOnInit(): void {
    this.getOpportunity();
  }

  getOpportunity(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.opportunityService.getOpportunity(id).subscribe(
      (opportunity) => {
        this.opportunity = opportunity;
      },
      (response) => {
        response.error.errors.forEach((e) => {
          this.messageService.add(e.errorMessage);
        });
      }
    );
  }
}
