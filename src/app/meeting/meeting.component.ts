import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { Meeting } from '../models/meeting';
import { Page } from '../models/page';
import { MeetingService } from '../services/meeting.service';
import { MessageService } from '../services/message.service';
import { PageEvent } from '@angular/material/paginator';

@Component({
  selector: 'app-meeting',
  templateUrl: './meeting.component.html',
  styleUrls: ['./meeting.component.scss']
})
export class MeetingComponent implements OnInit {
  @Input() parentId: string;
  meetings: Page<Meeting>;
  sortParam = 'dateStart';
  pageIndex = 0;
  pageSize = 10;
  pageSizeOptions: number[] = [10, 30, 50, 100];
  pageEvent: PageEvent;

  constructor(
    private contactService: MeetingService,
    private messageService: MessageService
  ) {}

  ngOnInit(): void {
    this.getMeetings();
  }

  getMeetings() {
    this.contactService.getMeetings(this.parentId, this.sortParam, this.pageIndex, this.pageSize, false).subscribe(
      meetings => {
        this.meetings = meetings;
      },
      response => {
        response.error.errors.forEach(e => {
          this.messageService.add(e.errorMessage);
        });
      }
    );
  }

  updatePage(event?: PageEvent) {
    this.pageIndex = event?.pageIndex;
    this.pageSize = event?.pageSize;
    this.getMeetings();
    return event;
  }

}
