import { Component, OnInit, Input } from '@angular/core';
import { Contact } from '../models/contact';
import { Page } from '../models/page';
import { ContactService } from '../services/contact.service';
import { MessageService } from '../services/message.service';
import { PageEvent } from '@angular/material/paginator';
import { buildAddressCityStateZipcode } from '../util/common.util';

@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.scss']
})
export class ContactsComponent implements OnInit {
  @Input() parentId: string;
  contacts: Page<Contact>;
  sortParam = 'last_name';
  pageIndex = 0;
  pageSize = 10;
  pageSizeOptions: number[] = [10, 30, 50, 100];
  pageEvent: PageEvent;
  buildAddress = buildAddressCityStateZipcode;

  constructor(
    private contactService: ContactService,
    private messageService: MessageService
  ) {}

  ngOnInit(): void {
    this.getContacts();
  }

  getContacts() {
    this.contactService.searchContacts(undefined, this.parentId, this.sortParam, this.pageIndex, this.pageSize, true).subscribe(
      contacts => {
        this.contacts = contacts;
      },
      response => {
        response.error.errors.forEach(e => {
          this.messageService.add(e.errorMessage);
        });
      }
    );
  }

  detailArray(account: object): [string, any][] {
    return Object.entries(account);
  }

  updatePage(event?: PageEvent) {
    this.pageIndex = event?.pageIndex;
    this.pageSize = event?.pageSize;
    this.getContacts();
    return event;
  }
}
