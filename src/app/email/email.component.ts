import { Component, OnInit, Input } from '@angular/core';
import { Email } from '../models/email';
import { Page } from '../models/page';
import { MessageService } from '../services/message.service';
import { EmailService } from '../services/email.service';
import { PageEvent } from '@angular/material/paginator';

@Component({
  selector: 'app-email',
  templateUrl: './email.component.html',
  styleUrls: ['./email.component.scss']
})
export class EmailComponent implements OnInit {
  @Input() parentId: string;
  emails: Page<Email>;
  sortParam = 'dateSent';
  pageIndex = 0;
  pageSize = 10;
  pageSizeOptions: number[] = [10, 30, 50, 100];
  pageEvent: PageEvent;


  constructor(
    private contactService: EmailService,
    private messageService: MessageService
  ) {}

  ngOnInit(): void {
    this.getEmails();
  }

  getEmails() {
    this.contactService.getEmails(this.parentId, this.sortParam, this.pageIndex, this.pageSize, false).subscribe(
      emails => {
        this.emails = emails;
      },
      response => {
        response.error.errors.forEach(e => {
          this.messageService.add(e.errorMessage);
        });
      }
    );
  }

  updatePage(event?: PageEvent) {
    this.pageIndex = event?.pageIndex;
    this.pageSize = event?.pageSize;
    this.getEmails();
    return event;
  }

}
