import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AccountsComponent } from './accounts/accounts.component';
import { AccountDetailComponent } from './account-detail/account-detail.component';
import { LoginComponent } from './login/login.component';
import { AuthGuard } from './services/auth.guard';
import { ContactListComponent } from './contact-list/contact-list.component';
import { ContactDetailComponent } from './contact-detail/contact-detail.component';
import { ErrorComponent } from './error/error.component';
import { OpportunityDetailComponent } from './opportunity-detail/opportunity-detail.component';
import { OpportunityListComponent } from './opportunity-list/opportunity-list.component';

const routes: Routes = [
  { path: '', redirectTo: '/accounts', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'accounts', component: AccountsComponent, canActivate: [ AuthGuard ] },
  { path: 'contacts', component: ContactListComponent, canActivate: [ AuthGuard ] },
  { path: 'opportunities', component: OpportunityListComponent, canActivate: [ AuthGuard ] },
  { path: 'accounts/:id', component: AccountDetailComponent, canActivate: [ AuthGuard ] },
  { path: 'contacts/:id', component: ContactDetailComponent, canActivate: [ AuthGuard ] },
  { path: 'opportunity/:id', component: OpportunityDetailComponent, canActivate: [ AuthGuard ] },
  { path: 'error', component: ErrorComponent },
  { path: '**', redirectTo: 'error', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
