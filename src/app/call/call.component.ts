import { Component, OnInit, Input } from '@angular/core';

import { Call } from '../models/call';
import { Page } from '../models/page';
import { CallService } from '../services/call.service';
import { MessageService } from '../services/message.service';
import { PageEvent } from '@angular/material/paginator';

@Component({
  selector: 'app-call',
  templateUrl: './call.component.html',
  styleUrls: ['./call.component.scss']
})
export class CallComponent implements OnInit {
  @Input() parentId: string;
  calls: Page<Call>;
  sortParam = 'dateStart';
  pageIndex = 0;
  pageSize = 10;
  pageSizeOptions: number[] = [10, 30, 50, 100];
  pageEvent: PageEvent;

  constructor(
    private contactService: CallService,
    private messageService: MessageService
  ) {}

  ngOnInit(): void {
    this.getCalls();
  }

  getCalls() {
    this.contactService.getCalls(this.parentId, this.sortParam, this.pageIndex, this.pageSize, false).subscribe(
      calls => {
        this.calls = calls;
      },
      response => {
        response.error.errors.forEach(e => {
          this.messageService.add(e.errorMessage);
        });
      }
    );
  }

  updatePage(event?: PageEvent) {
    this.pageIndex = event?.pageIndex;
    this.pageSize = event?.pageSize;
    this.getCalls();
    return event;
  }

}
