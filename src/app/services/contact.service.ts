import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, of } from 'rxjs';

import { Contact } from '../models/contact';
import { MessageService } from './message.service';
import { environment } from '../../environments/environment';
import { Page } from '../models/page';
import { fakeContacts } from '../util/mock-data.util';
import { addMarkToUrl } from '../util/common.util';
import { getUrl } from '../util/common.util';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
  withCredentials: true,
};

@Injectable({ providedIn: 'root' })
export class ContactService {
  constructor(
    private http: HttpClient,
    private messageService: MessageService
  ) {}

  getContact(id: string): Observable<Contact> {
    return this.http.get<Contact>(
      `${environment.apiUrl}/contact/${id}`,
      httpOptions
    );
  }

  searchContacts(
    contactName: string,
    accountId: string,
    sortParam: string,
    pageNumber: number,
    pageSize: number,
    ascending: boolean
  ): Observable<Page<Contact>> {
    const url = getUrl(
      contactName,
      accountId,
      sortParam,
      pageNumber,
      pageSize,
      ascending
    );
    return this.http.get<Page<Contact>>(url, httpOptions);
  }

  getUrl(
    contactName: string,
    accountId: string,
    sortParam: string,
    pageNumber: number,
    pageSize: number,
    ascending: boolean
  ): string {
    let result = `${environment.apiUrl}/contact`;
    let counter = 0;
    if (contactName) {
      result += `?contactName=${contactName}`;
      counter++;
    }
    if (accountId) {
      result += addMarkToUrl(counter);
      result += `accountId=${accountId}`;
      counter++;
    }
    if (sortParam) {
      result += addMarkToUrl(counter);
      result += `sortParam=${sortParam}`;
      counter++;
    }
    if (pageNumber || pageNumber === 0) {
      result += addMarkToUrl(counter);
      result += `pageNumber=${pageNumber}`;
      counter++;
    }
    if (pageSize) {
      result += addMarkToUrl(counter);
      result += `pageSize=${pageSize}`;
      counter++;
    }
    if (ascending) {
      result += addMarkToUrl(counter);
      result += `ascending=${ascending}`;
      counter++;
    }
    return result;
  }
}
