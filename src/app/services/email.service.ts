import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, of } from 'rxjs';

import { Email } from '../models/email';
import { MessageService } from './message.service';
import { environment } from '../../environments/environment';
import { Page } from '../models/page';
import { fakeEmails } from '../util/mock-data.util';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
  withCredentials: true,
};

@Injectable({ providedIn: 'root' })
export class EmailService {
  constructor(
    private http: HttpClient,
    private messageService: MessageService
  ) {}

  getEmails(
    parentId: string,
    sortParam: string,
    pageNumber: number,
    pageSize: number,
    ascending: boolean
  ): Observable<Page<Email>> {
    return this.http.get<Page<Email>>(
      `${environment.apiUrl}/email?parentId=${parentId}&sortParam=${sortParam}&pageSize=${pageSize}&pageNumber=${pageNumber}&ascending=${ascending}`,
      httpOptions
    );
  }
}
