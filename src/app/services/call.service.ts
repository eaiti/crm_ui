import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, of } from 'rxjs';

import { Call } from '../models/call';
import { MessageService } from './message.service';
import { environment } from '../../environments/environment';
import { Page } from '../models/page';
import { fakeCalls } from '../util/mock-data.util';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
  withCredentials: true,
};

@Injectable({ providedIn: 'root' })
export class CallService {
  constructor(
    private http: HttpClient,
    private messageService: MessageService
  ) {}

  getCalls(
    parentId: string,
    sortParam: string,
    pageNumber: number,
    pageSize: number,
    ascending: boolean
  ): Observable<Page<Call>> {
    return this.http.get<Page<Call>>(
      `${environment.apiUrl}/call?parentId=${parentId}&sortParam=${sortParam}&pageSize=${pageSize}&pageNumber=${pageNumber}&ascending=${ascending}`,
      httpOptions
    );
  }
}
