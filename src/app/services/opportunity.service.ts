import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, of } from 'rxjs';

import { Opportunity } from '../models/opportunity';
import { MessageService } from './message.service';
import { environment } from '../../environments/environment';
import { Page } from '../models/page';
import { addMarkToUrl } from '../util/common.util';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
  withCredentials: true,
};

@Injectable({ providedIn: 'root' })
export class OpportunityService {
  constructor(
    private http: HttpClient,
    private messageService: MessageService
  ) {}

  getOpportunity(id: string): Observable<Opportunity> {
    return this.http.get<Opportunity>(
      `${environment.apiUrl}/opportunity/${id}`,
      httpOptions
    );
  }

  getOpportunities(
    opportunityName: string,
    salesRepName: string,
    accountId: string,
    contactId: string,
    accountName: string,
    salesStage: string[],
    sortParam: string,
    pageNumber: number,
    pageSize: number,
    ascending: boolean
  ): Observable<Page<Opportunity>> {
    const url = this.getUrl(
      opportunityName,
      salesRepName,
      accountId,
      contactId,
      accountName,
      salesStage,
      sortParam,
      pageNumber,
      pageSize,
      ascending
    );
    return this.http.get<Page<Opportunity>>(url, httpOptions);
  }

  getUrl(
    opportunityName: string,
    salesRepName: string,
    accountId: string,
    contactId: string,
    accountName: string,
    salesStage: string[],
    sortParam: string,
    pageNumber: number,
    pageSize: number,
    ascending: boolean
  ): string {
    let result = `${environment.apiUrl}/opportunity`;
    let counter = 0;
    if (opportunityName) {
      result += addMarkToUrl(counter);
      result += `opportunityName=${opportunityName}`;
      counter++;
    }
    if (salesRepName) {
      result += addMarkToUrl(counter);
      result += `salesRepName=${salesRepName}`;
      counter++;
    }
    if (accountId) {
      result += addMarkToUrl(counter);
      result += `accountId=${accountId}`;
      counter++;
    }
    if (contactId) {
      result += addMarkToUrl(counter);
      result += `contactId=${contactId}`;
      counter++;
    }
    if (accountName) {
      result += addMarkToUrl(counter);
      result += `accountName=${accountName}`;
      counter++;
    }
    if (salesStage && salesStage.length !== 0) {
      for (const stage of salesStage) {
        result += addMarkToUrl(counter);
        result += `salesStages=${stage}`;
        counter++;
      }
    }
    if (sortParam) {
      result += addMarkToUrl(counter);
      result += `sortParam=${sortParam}`;
      counter++;
    }
    if (pageNumber !== undefined && pageNumber !== null) {
      result += addMarkToUrl(counter);
      result += `pageNumber=${pageNumber}`;
      counter++;
    }
    if (pageSize !== undefined && pageSize !== null) {
      result += addMarkToUrl(counter);
      result += `pageSize=${pageSize}`;
      counter++;
    }
    if (ascending !== undefined && ascending !== null) {
      result += addMarkToUrl(counter);
      result += `ascending=${ascending}`;
      counter++;
    }
    return result;
  }
}
