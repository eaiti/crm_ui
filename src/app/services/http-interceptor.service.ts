import { Injectable } from '@angular/core';
import { HttpHandler, HttpInterceptor, HttpRequest, HttpEvent, HttpResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { AuthenticationService } from './authentication.service';
import { MessageService } from './message.service';

@Injectable({
  providedIn: 'root'
})
export class HttpInterceptorService implements HttpInterceptor {

    constructor(
      private router: Router,
      private authenticationService: AuthenticationService,
      private messageService: MessageService) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
      return next.handle(request).pipe(
        catchError(error => {
          if (error.status === 403) {
            this.authenticationService.logout();
            this.router.navigate(['/login']);
            this.messageService.add('Session has expired. Please log in again.');
            return throwError(error);
          }
          return throwError(error);
        })
      );
    }
}
