import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, of } from 'rxjs';

import { Account } from '../models/account';
import { MessageService } from './message.service';
import { environment } from '../../environments/environment';
import { Page } from '../models/page';
import { fakeAccounts } from '../util/mock-data.util';
import { addMarkToUrl } from '../util/common.util';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
  withCredentials: true,
};

@Injectable({ providedIn: 'root' })
export class AccountService {
  constructor(
    private http: HttpClient,
    private messageService: MessageService
  ) {}

  getAccount(id: string): Observable<Account> {
    return this.http.get<Account>(
      `${environment.apiUrl}/account/${id}`,
      httpOptions
    );
  }

  searchAccounts(
    accountName: string,
    salesRepName: string,
    sortParam: string,
    pageNumber: number,
    pageSize: number,
    ascending: boolean
  ): Observable<Page<Account>> {
    const url = this.getUrl(
      accountName,
      salesRepName,
      sortParam,
      pageNumber,
      pageSize,
      ascending
    );
    return this.http.get<Page<Account>>(url, httpOptions);
  }

  getUrl(
    accountName: string,
    salesRepName: string,
    sortParam: string,
    pageNumber: number,
    pageSize: number,
    ascending: boolean
  ): string {
    let result = `${environment.apiUrl}/account`;
    let counter = 0;
    if (accountName) {
      result += `?accountName=${accountName}`;
      counter++;
    }
    if (salesRepName) {
      result += addMarkToUrl(counter);
      result += `salesRepName=${salesRepName}`;
      counter++;
    }
    if (sortParam) {
      result += addMarkToUrl(counter);
      result += `sortParam=${sortParam}`;
      counter++;
    }
    if (pageNumber !== undefined && pageNumber !== null) {
      result += addMarkToUrl(counter);
      result += `pageNumber=${pageNumber}`;
      counter++;
    }
    if (pageSize !== undefined && pageSize !== null) {
      result += addMarkToUrl(counter);
      result += `pageSize=${pageSize}`;
      counter++;
    }
    if (ascending !== undefined && ascending !== null) {
      result += addMarkToUrl(counter);
      result += `ascending=${ascending}`;
      counter++;
    }
    return result;
  }
}
