import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, of } from 'rxjs';

import { Meeting } from '../models/meeting';
import { MessageService } from './message.service';
import { environment } from '../../environments/environment';
import { Page } from '../models/page';
import { fakeMeetings } from '../util/mock-data.util';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
  withCredentials: true,
};

@Injectable({ providedIn: 'root' })
export class MeetingService {
  constructor(
    private http: HttpClient,
    private messageService: MessageService
  ) {}

  getMeetings(
    parentId: string,
    sortParam: string,
    pageNumber: number,
    pageSize: number,
    ascending: boolean
  ): Observable<Page<Meeting>> {
    return this.http.get<Page<Meeting>>(
      `${environment.apiUrl}/meeting?parentId=${parentId}&sortParam=${sortParam}&pageSize=${pageSize}&pageNumber=${pageNumber}&ascending=${ascending}`,
      httpOptions
    );
  }
}
