import { Component, OnInit } from '@angular/core';

import { Account } from '../models/account';
import { AccountService } from '../services/account.service';
import { Observable, BehaviorSubject } from 'rxjs';
import { Page } from '../models/page';
import {
  debounceTime,
  distinctUntilChanged,
  switchMap
} from 'rxjs/operators';
import { PageEvent } from '@angular/material/paginator';

@Component({
  selector: 'app-accounts',
  templateUrl: './accounts.component.html',
  styleUrls: ['./accounts.component.scss']
})
export class AccountsComponent implements OnInit {
  accounts: Page<Account>;
  private searchTerms = new BehaviorSubject<string>('');

  radioOption: 'account' | 'salesperson' = 'account';

  length: number;
  previousPageIndex: number;
  sortParam = 'name';
  pageIndex = 0;
  pageSize = 10;
  pageSizeOptions: number[] = [10, 30, 50, 100];

  pageEvent: PageEvent;

  loading = true;

  constructor(private accountService: AccountService) {}

  search(term: string): void {
    this.searchTerms.next(term);
    this.pageIndex = 0;
  }

  onRadioButtonChange() {
    this.pageIndex = 0;
    this.fetchAccounts();
  }

  updatePage(event?: PageEvent) {
    this.pageIndex = event?.pageIndex;
    this.pageSize = event?.pageSize;
    this.length = event?.length;
    this.previousPageIndex = event?.previousPageIndex;
    this.fetchAccounts();
    return event;
  }

  setPageSizeOptions(setPageSizeOptionsInput: string) {
    if (setPageSizeOptionsInput) {
      this.pageSizeOptions = setPageSizeOptionsInput
        .split(',')
        .map(str => +str);
    }
  }

  ngOnInit(): void {
    this.fetchAccounts();
  }

  fetchAccounts() {
    this.searchTerms
      .pipe(
        debounceTime(300),

        distinctUntilChanged(),

        switchMap((searchString: string) => {
          this.loading = true;
          if (this.radioOption === 'account') {
            return this.accountService.searchAccounts(
              searchString,
              null,
              this.sortParam,
              this.pageIndex,
              this.pageSize,
              true
            );
          } else {
            return this.accountService.searchAccounts(
              null,
              searchString,
              this.sortParam,
              this.pageIndex,
              this.pageSize,
              true
            );
          }
        })
      )
      .subscribe(accounts => {
          this.accounts = accounts;
          this.loading = false;
      });
  }

  buildCityState(city: string, state: string): string {
    return state ? `${city}, ${state}` : city;
  }
}
