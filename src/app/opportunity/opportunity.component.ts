import { Component, OnInit, Input } from '@angular/core';
import { Opportunity } from '../models/opportunity';
import { Page } from '../models/page';
import { MessageService } from '../services/message.service';
import { OpportunityService } from '../services/opportunity.service';
import { PageEvent } from '@angular/material/paginator';
import { buildSalesRepName } from '../util/common.util';

@Component({
  selector: 'app-opportunity',
  templateUrl: './opportunity.component.html',
  styleUrls: ['./opportunity.component.scss'],
})
export class OpportunityComponent implements OnInit {
  @Input() accountId: string;
  @Input() contactId: string;
  salesStage: string[];
  sortParam = 'name';
  opportunities: Page<Opportunity>;
  pageIndex = 0;
  pageSize = 10;
  pageSizeOptions: number[] = [10, 30, 50, 100];
  pageEvent: PageEvent;
  salesStages = [
    'Closed Lost',
    'Prospecting',
    'Negotiating',
    'Closed Won',
    'Presenting',
    'Qualifying',
    'Target',
  ];

  buildSalesRepName = buildSalesRepName;

  constructor(
    private opportunityService: OpportunityService,
    private messageService: MessageService
  ) {}

  ngOnInit(): void {
    this.getOpportunities();
  }

  getOpportunities() {
    this.opportunityService
      .getOpportunities(
        null,
        null,
        this.accountId,
        this.contactId,
        null,
        this.salesStage,
        this.sortParam,
        this.pageIndex,
        this.pageSize,
        true
      )
      .subscribe(
        (opportunities) => {
          this.opportunities = opportunities;
        },
        (response) => {
          response.error.errors.forEach((e) => {
            this.messageService.add(e.errorMessage);
          });
        }
      );
  }

  updatePage(event?: PageEvent) {
    this.pageIndex = event?.pageIndex;
    this.pageSize = event?.pageSize;
    this.getOpportunities();
    return event;
  }
}
