import { environment } from '../../environments/environment';
import { SalesRep } from '../models/sales.rep';

export function buildAddressCityStateZipcode(city?: string, state?: string, zipcode?: string): string {
  let address = '';
  if (city) {
    address += city;
  }
  if (state) {
    address += address ? `, ${state}` : `${state}`;
  }
  if (zipcode) {
    address += address ? ` ${zipcode}` : `${zipcode}`;
  }
  return address;
}

export function addMarkToUrl(counter: number): string {
  return counter === 0 ? '?' : '&';
}

export function getUrl(
  contactName: string,
  accountId: string,
  sortParam: string,
  pageNumber: number,
  pageSize: number,
  ascending: boolean
): string {
  let result = `${environment.apiUrl}/contact`;
  let counter = 0;
  if (contactName) {
    result += `?contactName=${contactName}`;
    counter++;
  }
  if (accountId) {
    result += addMarkToUrl(counter);
    result += `accountId=${accountId}`;
    counter++;
  }
  if (sortParam) {
    result += addMarkToUrl(counter);
    result += `sortParam=${sortParam}`;
    counter++;
  }
  if (pageNumber || pageNumber === 0) {
    result += addMarkToUrl(counter);
    result += `pageNumber=${pageNumber}`;
    counter++;
  }
  if (pageSize) {
    result += addMarkToUrl(counter);
    result += `pageSize=${pageSize}`;
    counter++;
  }
  if (ascending) {
    result += addMarkToUrl(counter);
    result += `ascending=${ascending}`;
    counter++;
  }
  return result;
}

export function buildSalesRepName(salesRep: SalesRep): string {
  return `${salesRep.firstName} ${salesRep.lastName}`.trim();
}
