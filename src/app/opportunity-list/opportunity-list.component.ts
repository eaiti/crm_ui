import { Component, OnInit } from "@angular/core";
import { Opportunity } from "../models/opportunity";
import { Page } from "../models/page";
import { BehaviorSubject } from "rxjs";
import { OpportunityService } from "../services/opportunity.service";
import { PageEvent } from "@angular/material/paginator";
import { debounceTime, distinctUntilChanged, switchMap } from "rxjs/operators";
import { buildSalesRepName } from "../util/common.util";
import { MessageService } from "../services/message.service";

@Component({
  selector: "app-opportunity-list",
  templateUrl: "./opportunity-list.component.html",
  styleUrls: ["./opportunity-list.component.scss"],
})
export class OpportunityListComponent implements OnInit {
  opportunities: Page<Opportunity>;

  buildSalesRepName = buildSalesRepName;
  private searchTerms = new BehaviorSubject<string>("");

  radioOption: "account" | "salesperson" | "opportunity" = "opportunity";

  length: number;
  previousPageIndex: number;
  sortParam = "name";
  pageIndex = 0;
  pageSize = 10;
  pageSizeOptions: number[] = [10, 30, 50, 100];

  pageEvent: PageEvent;

  loading = true;

  salesStage: string[];
  salesStages = [
    "Closed Lost",
    "Prospecting",
    "Negotiating",
    "Closed Won",
    "Presenting",
    "Qualifying",
    "Target",
  ];

  constructor(
    private opportunityService: OpportunityService,
    private messageService: MessageService
  ) {}

  search(term: string): void {
    this.searchTerms.next(term);
    this.pageIndex = 0;
  }

  onRadioButtonChange() {
    this.pageIndex = 0;
    this.fetchOpportunities();
  }

  ngOnInit(): void {
    this.fetchOpportunities();
  }

  updatePage(event?: PageEvent) {
    this.pageIndex = event?.pageIndex;
    this.pageSize = event?.pageSize;
    this.length = event?.length;
    this.previousPageIndex = event?.previousPageIndex;
    this.fetchOpportunities();
    return event;
  }

  setPageSizeOptions(setPageSizeOptionsInput: string) {
    if (setPageSizeOptionsInput) {
      this.pageSizeOptions = setPageSizeOptionsInput
        .split(",")
        .map((str) => +str);
    }
  }

  fetchOpportunities() {
    this.searchTerms
      .pipe(
        debounceTime(300),

        distinctUntilChanged(),

        switchMap((searchString: string) => {
          this.loading = true;
          if (this.radioOption === "account") {
            return this.opportunityService.getOpportunities(
              null,
              null,
              null,
              null,
              searchString,
              this.salesStage,
              this.sortParam,
              this.pageIndex,
              this.pageSize,
              true
            );
          } else if (this.radioOption === "salesperson") {
            return this.opportunityService.getOpportunities(
              null,
              searchString,
              null,
              null,
              null,
              this.salesStage,
              this.sortParam,
              this.pageIndex,
              this.pageSize,
              true
            );
          } else {
            return this.opportunityService.getOpportunities(
              searchString,
              null,
              null,
              null,
              null,
              this.salesStage,
              this.sortParam,
              this.pageIndex,
              this.pageSize,
              true
            );
          }
        })
      )
      .subscribe((opportunities) => {
        this.opportunities = opportunities;
        this.loading = false;
      });
  }
}
