import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Contact } from '../models/contact';
import { Page } from '../models/page';
import { Meeting } from '../models/meeting';
import { ActivatedRoute } from '@angular/router';
import { MessageService } from '../services/message.service';
import { ContactService } from '../services/contact.service';
import { buildAddressCityStateZipcode } from '../util/common.util';

@Component({
  selector: 'app-contact-detail',
  templateUrl: './contact-detail.component.html',
  styleUrls: ['./contact-detail.component.scss']
})
export class ContactDetailComponent implements OnInit {

  contact: Contact;
  buildAddress = buildAddressCityStateZipcode;

  constructor(
    private route: ActivatedRoute,
    private messageService: MessageService,
    private contactService: ContactService,
    private location: Location
  ) {}

  ngOnInit(): void {
    this.getContact();
  }

  getContact(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.contactService.getContact(id).subscribe(
      contact => {
        this.contact = contact;
      },
      response => {
        response.error.errors.forEach(e => {
          this.messageService.add(e.errorMessage);
        });
      }
    );
  }
}
