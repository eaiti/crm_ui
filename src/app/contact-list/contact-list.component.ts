import { Component, OnInit } from '@angular/core';
import { Contact } from '../models/contact';
import { Page } from '../models/page';
import { Observable, BehaviorSubject } from 'rxjs';
import { PageEvent } from '@angular/material/paginator';
import { ContactService } from '../services/contact.service';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { buildAddressCityStateZipcode } from '../util/common.util';

@Component({
  selector: 'app-contact-list',
  templateUrl: './contact-list.component.html',
  styleUrls: ['./contact-list.component.scss']
})
export class ContactListComponent implements OnInit {
  buildAddress = buildAddressCityStateZipcode;
  contacts: Page<Contact>;
  private searchTerms = new BehaviorSubject<string>('');

  length: number;
  previousPageIndex: number;
  sortParam = 'last_name';
  pageIndex = 0;
  pageSize = 10;
  pageSizeOptions: number[] = [10, 30, 50, 100];
  searchString: string;

  pageEvent: PageEvent;

  loading = true;

  constructor(private contactService: ContactService) {
  }

  search(term: string): void {
    this.searchTerms.next(term);
    this.pageIndex = 0;
  }

  ngOnInit(): void {
    this.fetchContacts();
  }

  updatePage(event?: PageEvent) {
    this.pageIndex = event?.pageIndex;
    this.pageSize = event?.pageSize;
    this.length = event?.length;
    this.previousPageIndex = event?.previousPageIndex;
    this.fetchContacts();
    return event;
  }

  setPageSizeOptions(setPageSizeOptionsInput: string) {
    if (setPageSizeOptionsInput) {
      this.pageSizeOptions = setPageSizeOptionsInput.split(',').map(str => +str);
    }
  }

  fetchContacts() {
    this.searchTerms.pipe(
      debounceTime(300),

      distinctUntilChanged(),

      switchMap((searchString: string) => {
        this.loading = true;
        this.searchString = searchString;
        return this.contactService.searchContacts(
          searchString, null, this.sortParam, this.pageIndex, this.pageSize, true
        );
      })
    ).subscribe(
      contacts => {
        this.contacts = contacts;
        this.loading = false;
      }
    );
  }
}
