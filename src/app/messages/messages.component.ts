import { Component, OnInit } from '@angular/core';
import { MessageService } from '../services/message.service';
import { Router, NavigationEnd, Event } from '@angular/router';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.scss']
})
export class MessagesComponent implements OnInit {

  constructor(public messageService: MessageService, private router: Router) {
    router.events.subscribe( (event: Event) => {
      if (event instanceof NavigationEnd) {
        this.messageService.clear();
      }
    });
  }

  ngOnInit(): void {
  }
}
